import Vue from 'vue';
import Router from 'vue-router';
import Meta from 'vue-meta';
import Hall from '@/components/Hall';
import About from '@/components/About';

Vue.use(Router);
Vue.use(Meta);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'About',
      component: About,
    },
    {
      path: '/hall',
      name: 'Hall',
      component: Hall,
    },
  ],
});
