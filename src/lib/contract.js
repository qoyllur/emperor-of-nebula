const Neb = require('nebulas').Neb;
const Account = require('nebulas').Account;
const HttpRequest = require('nebulas').HttpRequest;

const neb = new Neb();
neb.setRequest(new HttpRequest('https://mainnet.nebulas.io'));
const account = Account.NewAccount();

/* global alert */
/* eslint no-console: ["error", { allow: ["warn", "error"] }] */

class SmartContract {
  constructor(address) {
    this.neb = neb;
    this.chainId = 1;
    this.address = address;
  }

  check(fun, args, callback) {
    const params = {};

    params.from = account.getAddressString();
    params.to = this.address;
    params.gasLimit = 200000;
    params.gasPrice = 1000000;
    params.value = 0;
    params.contract = { function: fun, args: JSON.stringify(args) };

    this.neb.api.getAccountState(params.from).then((accStateResp) => {
      params.nonce = parseInt(accStateResp.nonce, 10) + 1;

      this.neb.api.call({
        from: params.from,
        to: params.to,
        value: params.value,
        nonce: params.nonce,
        gasPrice: params.gasPrice,
        gasLimit: params.gasLimit,
        contract: params.contract,
      }).then((callResp) => {
        callback(JSON.parse(callResp.result));
      }).catch((err) => {
        console.error(err);
      });
    }).catch((err) => {
      console.error(err);
    });
  }
}

export default { SmartContract };
